(define (WARA-all-commented? str)
  (= 0 (rxmatch-num-matches (rxmatch #/^(\s*[^\/]*|\s*[^\/]{2}.*)$/ str))))

(define (WARA-toggle-comment-row row)
  (let ((line (editor-get-row-string row)))
    (editor-set-select-area row 0 row 10000)
    (editor-paste-string 
      (if (WARA-all-commented? line)
        (regexp-replace #/^(\s*)\/\/(.*)$/ str "$1$2")
        (string-append "//" str)))))

(define (WARA-toggle-comment)
  (if (eq? #f (editor-get-selected-area))
    (WARA-toggle-comment-row (editor-get-cur-row))
    (let ((selected-area (editor-get-selected-string)))
      (if (WARA-all-commented? selected-area)
        (editor-replace-selected-string "^\s*//" "")
        (editor-replace-selected-string "^" "//")))))
